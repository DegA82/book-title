package com.example.bookapp.model

import android.content.Context
import com.example.bookapp.model.local.BookDatabase
import com.example.bookapp.model.local.entity.Book
import com.example.bookapp.model.remote.BookService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val TAG = "BookNerdRepo"

class BookNerdRepo @Inject constructor(
    private val bookService: BookService,
    @ApplicationContext context: Context
) {

    val bookDao = BookDatabase.getInstance(context).bookDao()

    suspend fun getBooks() = withContext(Dispatchers.IO){
        val cachedBooks: List<Book> = bookDao.getAll()

        return@withContext cachedBooks.ifEmpty {
            val books: List<Book> = bookService.getBooks().map {
                Book(title = it.title)
            }
                bookDao.insert(books)
            return@ifEmpty books
        }
    }
}