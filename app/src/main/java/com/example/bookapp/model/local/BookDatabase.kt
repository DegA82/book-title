package com.example.bookapp.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.bookapp.model.local.dao.BookDao
import com.example.bookapp.model.local.entity.Book

@Database(entities = [Book::class], version = 1)
abstract class BookDatabase: RoomDatabase() {

    abstract fun bookDao(): BookDao

    companion object{

        private const val DATABASE_NAME = "books.db"

        // For Singleton instantiation
        @Volatile
        private var instance: BookDatabase? = null

        fun getInstance(context: Context): BookDatabase{
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
        private fun buildDatabase(context: Context): BookDatabase {
            return Room.databaseBuilder(context, BookDatabase::class.java, DATABASE_NAME).build()
        }
    }

}