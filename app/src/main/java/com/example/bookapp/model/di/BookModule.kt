package com.example.bookapp.model.di

import com.example.bookapp.model.remote.BookService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object BookModule {

    @Provides
    fun providesBookService(): BookService = BookService.getInstance()
}