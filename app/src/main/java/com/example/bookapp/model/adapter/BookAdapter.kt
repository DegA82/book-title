package com.example.bookapp.model.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookapp.databinding.ItemBookBinding
import com.example.bookapp.model.local.entity.Book

class BookAdapter(
    private  val books: List<Book>
): RecyclerView.Adapter<BookAdapter.BookViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int
    )  = BookViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: BookAdapter.BookViewHolder, position: Int) {
        val book = books[position]
        holder.loadBook(book)
    }

    override fun getItemCount(): Int {
        return books.size
    }

    class BookViewHolder(
        private val binding: ItemBookBinding
    ): RecyclerView.ViewHolder(binding.root){

        fun loadBook(book: Book){
            with(binding){
                btnTv.text =book.title
            }

        }
    companion object{
        fun getInstance(parent: ViewGroup) = ItemBookBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ).let { binding -> BookViewHolder(binding) }
    }
    }



}