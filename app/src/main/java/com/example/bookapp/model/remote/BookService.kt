package com.example.bookapp.model.remote

import com.example.bookapp.model.local.entity.Book
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path

interface BookService {

    companion object{

        private const val BASE_URL = "https://the-dune-api.herokuapp.com/"

        fun getInstance() = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<BookService>()
    }

    @GET("/books/{number}")
    suspend fun getBooks(@Path("number") number:Int = 100): List<Book>
}