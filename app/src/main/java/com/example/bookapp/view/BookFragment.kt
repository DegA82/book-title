package com.example.bookapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.bookapp.databinding.FragmentBookBinding
import com.example.bookapp.model.adapter.BookAdapter
import com.example.bookapp.viewmodel.BookViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BookFragment: Fragment() {

    private var _binding: FragmentBookBinding? = null
    private val binding get() = _binding!!
//    @Inject lateinit var viewModel: BookViewModel
    private val bookViewModel by viewModels<BookViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBookBinding.inflate(inflater, container, false).also{_binding=it}.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bookViewModel.state.observe(viewLifecycleOwner){state ->
            binding.run {
                btnRv.adapter = BookAdapter(state.books)
            }

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}
